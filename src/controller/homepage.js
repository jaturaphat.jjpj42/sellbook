exports.homepage = class homepage{
    views(req,res){
        res.render('homepage')
    }
    senddata(req,res){
        let session = req.session
        session.data = req.body.Data
        session.cart = req.body.Cart
        res.send({Message:"success"})
    }
    confirm(req,res){
        let Data = req.session.data
        let Cart = req.session.cart
        let discount = 0
        let totalprice = 0
        let sum = 0 
        let total = 0
        let totalQty = 0
        let low = 0//ตัวเก็บจำนวนสินค้าที่จะลด
        if(Cart.length == 2){
            discount = 0.1
        }else if(Cart.length == 3){
            discount = 0.2
        }else if(Cart.length == 4){
            discount = 0.3
        }else if(Cart.length == 5){
            discount = 0.4
        }else if(Cart.length == 6){
            discount = 0.5
        }else if(Cart.length == 7){
            discount = 0.6
        }
        //หาจำนวนทั้งหมด
        for(let i = 0 ; i < Cart.length; i++){
            //เช็คว่่าหนังสือที่เข้ามามีจำนวนซ้ำกันมั้ย และ หาจำนวนที่หนังสือไม่ซ้ำกันเพื่อเอาไปคิดส่วนลด
            if(low == 0){
                //เช็ครอบแรกที่เข้ารับจำนวนเก็บไว้ก่อน
                low = Cart[i].quantity
            }else{
                //ถ้าเจอน้อยกว่าให้เก็บไว้เอามาคิดส่วนลด
                if(low > Cart[i].quantity){low = Cart[i].quantity}
            }
            totalprice += Cart[i].price*Cart[i].quantity
            totalQty += Cart[i].quantity
        }
        
        //หาส่วนลดโดยการให้ จำนวนหนังสือที่ไม่ซ้ำไปคูณกับราคาหนังสือ
        total = 100 * low;
        //นำ total ที่ได้มาไปคูณกับส่วนลดที่หนังสือไม่ซ้ำกัน
        total *= discount
        //นำ total ไปคูณกับจำนวนชนิดของหนังสือก็จะได้ส่วนลดมา
        sum = total*Cart.length;
        console.log(totalprice - sum, sum , totalprice);
        res.render('confirm',{Data:Data,Cart:Cart,discount:sum,totalPrice:totalprice,totalQty:totalQty})
    }
    ordersuccess(req,res){
        req.session.destroy();
        res.render('ordersuccess')
    }
}