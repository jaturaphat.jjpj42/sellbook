const express = require('express');
const app = express.Router();
const Homepage = require('../controller/homepage.js')



app.get('/',(req,res)=>{new Homepage.homepage().views(req,res)})
app.post('/senddata',(req,res)=>{new Homepage.homepage().senddata(req,res)})
app.get('/confirm',(req,res)=>{new Homepage.homepage().confirm(req,res)})
app.get('/ordersuccess',(req,res)=>{new Homepage.homepage().ordersuccess(req,res)})
module.exports = app;