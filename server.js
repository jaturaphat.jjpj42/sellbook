const express = require('express')
const session = require('express-session')
const server = express()
const oneDay = 1000 * 60 * 60 * 24;
const bodyParser = require('body-parser')
const passport = require('passport')
const flash = require('connect-flash')
const cookieParser = require('cookie-parser');
var morgan = require('morgan')



const homepage = require('./src/routers/homepage')


server.use(express.json())
server.set("views","./src/views")
server.set("view engine","ejs")
server.use(express.static('./src/public/'))

server.use(session({
    secret: "bannaydin",
    saveUninitialized: true,
    cookie: { maxAge: oneDay },
    resave: false
}));

server.use('/',homepage)


server.listen(3003,'localhost',()=>{
    console.log('test in port : 3003')
})